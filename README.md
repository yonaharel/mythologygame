### Mythology Outbreak  ###

 ![gameplay picture](/readme-picture.jpeg)


This is a game made in unity with C#, a simple platformer game with AI enemies and dumb enemies,
it has my own implemented item and inventory system.
The player can shoot magic or attack with a sword. He can equip different armors and different swords.
The game currently has Three levels. One for Nordic mythology, one for Egyptian mythology and one for Greek mythology.
Each level presents different tilemap and different enemies. 
