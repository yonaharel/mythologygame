﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float speed = 20f;
    public Rigidbody2D rb;
    public int damage = 10;
    public GameObject impactEffect;
    public LayerMask thingsToIgnore;
    public Vector3 bulletDirection; 

    // Start is called before the first frame update
    void Start()
    { 
             rb.velocity = bulletDirection * speed;
            // rb.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {

     

        if (IsInLayerMask(hitInfo.gameObject, thingsToIgnore)) return;

        FindObjectOfType<AudioManager>().Play("Spell Hit");
      
        Enemy enemy = hitInfo.GetComponent<Enemy>();
        if (enemy)
        {
           
            enemy.TakeDamage(damage);

        }

        if (hitInfo.name == "Hero")
        {
            hitInfo.GetComponent<PlayerCombat>().takeDamage(damage);
        }

        if (impactEffect)    //check if we set an Impact Effect.
            Instantiate(impactEffect, transform.position, transform.rotation);

        Destroy(gameObject);

    }

   


    public bool IsInLayerMask(GameObject obj, LayerMask layerMask)
    {
        return ((layerMask.value & (1 << obj.layer)) > 0);
    }
}
