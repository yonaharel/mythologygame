﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeAttack : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform attackPoint;
    private Animator animator;

    public float attackRate = 2f;
    private float nextAttackTime = 0f;

    public LayerMask heroLayer;
    public float attackRange;
    public int attackDamage;
    public float animationTime = 1;

    

    Enemy.EnemyType enemyType;

    void Start()
    {
        animator = GetComponent<Animator>();

        enemyType = GetComponent<Enemy>().enemyType;

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Time.time > nextAttackTime)
        {
            Attack();
            nextAttackTime = Time.time + attackRate;
        }

    }



    public void Attack()
    {
       
        // Collider2D[] hitColliders = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, heroLayer);
        animator.SetTrigger("Attack");
        switch(enemyType)
        {
            case Enemy.EnemyType.ICEMAN:
                FindObjectOfType<AudioManager>().Play("EnemyAttack");
                break;
            case Enemy.EnemyType.DEMON:
                FindObjectOfType<AudioManager>().Play("EnemyAttack2");
                break;
            case Enemy.EnemyType.SKELETON:
                FindObjectOfType<AudioManager>().Play("EnemyAttack3");
                break;
            case Enemy.EnemyType.HELL_BEAST:
                FindObjectOfType<AudioManager>().Play("EnemyAttack4");
                break;
            case Enemy.EnemyType.AXE_DEMON:
                FindObjectOfType<AudioManager>().Play("EnemyAttack5");
                break;
            case Enemy.EnemyType.MINOTAUR:
                FindObjectOfType<AudioManager>().Play("EnemyAttack2");
                break;
            case Enemy.EnemyType.WOLF:
                FindObjectOfType<AudioManager>().Play("EnemyAttack3");
                break;
            case Enemy.EnemyType.PATROL_WITCH:
            case Enemy.EnemyType.RANGED_STILL_WITCH:
                FindObjectOfType<AudioManager>().Play("EnemyAttack4");
                break;
            case Enemy.EnemyType.GOLEM:
                FindObjectOfType<AudioManager>().Play("EnemyAttack6");
                break;
            default:
                Debug.Log("Nothing");
                break;
        }

        //Collider2D playerCollider = Physics2D.OverlapCircle(attackPoint.position, attackRange, heroLayer);
        //if(playerCollider)
        //{

        //    PlayerCombat player = playerCollider.gameObject.GetComponent<PlayerCombat>();   
        //    player.takeDamage(attackDamage);
        //    Debug.Log("hit player");
        //}

        StartCoroutine(ActuallyAttack());

    }

    IEnumerator ActuallyAttack()
    {
        yield return new WaitForSeconds(animationTime);

        Collider2D playerCollider = Physics2D.OverlapCircle(attackPoint.position, attackRange, heroLayer);
        if (playerCollider)
        {

            PlayerCombat player = playerCollider.gameObject.GetComponent<PlayerCombat>();
            player.takeDamage(attackDamage);
            Debug.Log("hit player");
        }
    }


    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

}
