﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StillEnemy : MonoBehaviour
{

    private Animator animator;
    private EnemyRangedAttack rangedAttack;
    public float activationDistance = 10f;
    public Transform target;
    private bool m_attackingPlayer = false;
    public bool facingRight = true;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rangedAttack = GetComponent<EnemyRangedAttack>();
        target = GameObject.Find("Hero").transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (!rangedAttack) return;

        float distanceFromPlayer = Vector3.Distance(transform.position, target.position);
        if (distanceFromPlayer < activationDistance)
        {
            if (!m_attackingPlayer)
                AttackPlayer();
            return;
        } else
        {
            rangedAttack.enabled = false;
        }

    }


    void AttackPlayer()
    {

        m_attackingPlayer = true;
     
        //if player to the left of us
        if (target.position.x < transform.position.x)
        {
            // Switch the way the player is labelled as facing.
            if (facingRight)
                Flip();
        }
        else //player to the right of us
        {
            if (!facingRight)
                Flip();
        }

            rangedAttack.enabled = true;
        
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Rotate player
        transform.Rotate(0f, 180f, 0f);

        Debug.Log("flipping player");
    }

}
