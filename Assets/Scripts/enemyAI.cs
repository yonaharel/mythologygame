﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class enemyAI : MonoBehaviour
{

    public Transform target;
    public Transform EnemyGFX;
    float currDistanceFromTarget;
    public float speed = 200f;
    public float nextWaypointDistance = 3f;
    Path path;
    int currentWaypoint = 0;
    bool reachedEndOfPath = false;
    Seeker seeker;
    Rigidbody2D rb;

   [SerializeField] private Animator animator;

    public bool landCreature = false;

    // Start is called before the first frame update
    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("UpdatePath", 0f, .5f);
        animator = GetComponent<Animator>();

        if (!target)
        {
            target = GameObject.Find("Hero").transform;
        }
    }

    void UpdatePath()
    {
        
        if (seeker.IsDone())
            seeker.StartPath(rb.position, target.position, OnPathComplete);

    }
    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        } else
        {
            Debug.Log("path is error " + p.error);
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        
        

        if (path == null)
        {
            Debug.Log("path is null " + transform.name);
            animator.SetFloat("Speed", 0f);
            return;
        }
        
        if (currentWaypoint >= path.vectorPath.Count)
        {
            Debug.Log("reached end of path " + transform.name );
            reachedEndOfPath = true;
            animator.SetFloat("Speed", 0f);

            return;
        } else
        {
            reachedEndOfPath = false;
        }

        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;
        if (landCreature)
        {
            force.y = 0;
        }

        rb.AddForce(force);

        animator.SetFloat("Speed", speed);

     
        Debug.Log("Moving the object " + transform.name + "With foce:" + force);
        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);
        if(distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }

        //flipping
        if (force.x >= 0.01f)
        {
            EnemyGFX.localScale = new Vector3(-1f, 1f, 1f);
        }
        else if (force.x <= -0.01f)
        {
            EnemyGFX.localScale = new Vector3(1f, 1f, 1f);

        }

   
    }
}

    
