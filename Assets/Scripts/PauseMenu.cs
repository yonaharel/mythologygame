﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public static bool gameIsPaused = false;
    public GameObject pauseMenuUI;
    // Update is called once per frame

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            } else
            {
                
                Pause();
            }
        }

    }


    public void PauseButtonClick()
    {
        
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {

                Pause();
            }
        
    }

    public void Resume()
    {

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;

      
    }

    public void LoadMenu()
    {
        Debug.Log("loading menu..");

        Time.timeScale = 1f;

        //if we go back to menu there is no reason to keep gameobjects around,
        //so we destroy them

        FindObjectOfType<LevelLoader>().DestroyAll();

        SceneManager.LoadScene("Main Menu");

    }

    public void QuitGame()
    {
        Debug.Log("Quitting game..");
        Application.Quit();
    }
}
