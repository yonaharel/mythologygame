﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartEnemy : MonoBehaviour
{
    private enemyAI enemyAI;
    public float activationDistance = 20f;
    public bool ranged = false;
    private EnemyMeleeAttack meleeAttack;
    public Transform target;
    public float attackingDistance = 1f;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        enemyAI = GetComponent<enemyAI>();
        enemyAI.enabled = false;
        if(!ranged)
        {
            meleeAttack = GetComponent<EnemyMeleeAttack>();
            meleeAttack.enabled = false;
        }

        if (!target)
        {
            target = GameObject.Find("Hero").transform;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float distance = Vector2.Distance(transform.position, target.position);

        if (distance <= activationDistance)
        {
            if(distance < attackingDistance)
            {
                rb.velocity = Vector2.zero;
                meleeAttack.enabled = true;
                enemyAI.enabled = false;
                return;
            }
            else
            {
                meleeAttack.enabled = false;
                enemyAI.enabled = true;  
            }

         
        }
        else enemyAI.enabled = false;
    }
}
