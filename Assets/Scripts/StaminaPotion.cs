﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaPotion : Item
{

    public int staminaBonus = 20;

    public override void Use()
    {
        base.Use();
        player.GetStamina(staminaBonus);
    }

}
