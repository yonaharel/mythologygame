﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void PlayButtonPressed()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }


    public void ExitButtonPressed()
    {
        Debug.Log("Quitting the game.. ");
        Application.Quit();
    }

    public void SettingsButtonPressed()
    {

    }

}
