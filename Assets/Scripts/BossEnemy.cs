﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : MonoBehaviour
{

    public Collider2D gridCollider;
    private Enemy enemy;


    private void Start()
    {
        gridCollider.enabled = false;
    }

    private void Update()
    {

        enemy = GetComponent<Enemy>();


        if (enemy.m_isDead)
        {
            gridCollider.enabled = true;
        }

    }



}
