﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordItem : Item
{
   
    public override void Use()
    {
       base.Use();
       player.SetSword(GetComponent<Sword>(), this);
    }
}
