﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndCutScene : MonoBehaviour
{
    float countdown = 0f;
    public LevelLoader levelLoader;

    // Start is called before the first frame update
    void Start()
    {
        countdown = 8f;
        StartCoroutine(FinishedCutScene());
      
    }

 

    IEnumerator FinishedCutScene()
    {
        yield return new WaitForSeconds(countdown);
        if(SceneManager.GetActiveScene().buildIndex == 7)
        {
            FindObjectOfType<LevelLoader>().LoadFirstLevel();
        }

        FindObjectOfType<LevelLoader>().FinishCutScene();

       

    }
}
