﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{

    public Transform animationPosition;

    //public List<GameObject> animations;
    public GameObject healAnimation;
    public GameObject manaAnimation;
    public GameObject hurtAnimation;


    public void PlayHealAnimation()
    {

        if (healAnimation)
        {
            Instantiate(healAnimation, animationPosition.position, animationPosition.rotation);
        }

    }

    public void PlayManaAnimation()
    {

        if (healAnimation)
        {
            Instantiate(manaAnimation, animationPosition.position, animationPosition.rotation);
        }

    }

}
