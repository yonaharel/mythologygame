﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public static GameMaster GM;

    public List<Item> items;

    public GameObject hero;
    public GameObject canvas;
    public GameObject blood;

    public Vector3 egyptStartPosition = new Vector3(-73.2f, -16.4f, 0f);
    public Vector3 greekStartPosition = new Vector3(-12.7f, -2.3f, 0f);


    void Awake()
    {
        if (GM != null)
            GameObject.Destroy(GM);
        else
            GM = this;

       // DontDestroyOnLoad(this);
    }
}