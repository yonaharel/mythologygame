﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    //movement with CharacterController.

    public float runSpeed = 50f;
    public Animator animator;
    public Joystick joystick;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    public CharacterController controller;
    private Vector3 startPosition;

    private float jumpTimeCounter;
    public float jumpTime;

    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private float playerSpeed = 2.0f;
    private float jumpHeight = 1.0f;
    private float gravityValue = -9.81f;

    void Start()
    {
        startPosition = transform.position;
        //Physics2D.IgnoreLayerCollision(8, 13, true);

        Debug.Log("Start Player");

        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        //Input
        horizontalMove = joystick.Horizontal * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));



        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 move = new Vector3(horizontalMove, 0, 0);
      //  controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        // Changes the height position of the player..
        if (Input.GetButtonDown("Jump") && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
       // controller.Move(playerVelocity * Time.deltaTime);

    }

    public void OnLanding()
    {
        animator.SetBool("isJumping", false);
    }

    public void JumpButtonClicked()
    {
        Debug.Log("jump clicked");
        animator.SetBool("isJumping", true);

        if (controller.isGrounded)
            jump = true;
        else
        {
            jump = false;
        }
    }

    public void JumpButtonPressed()
    {
        Debug.Log("hodling jump button");


        if (controller.isGrounded && !jump)
        {
            FindObjectOfType<AudioManager>().Play("PlayerJump");

            jump = true;
        }

        if (jump)
        {
            if (jumpTimeCounter > 0)
                jumpTimeCounter -= Time.deltaTime;
            else jump = false;
        }
    }

    public void JumpButtonReleased()
    {

        Debug.Log("released jump button");
        jump = false;
    }

    public void CrouchButtonPressed()
    {
        crouch = true;

    }

    public void CrouchButtnoReleased()
    {
        crouch = false;
    }


    private void FixedUpdate()
    {
        animator.SetBool("isCrouching", crouch);

        animator.SetBool("isJumping", jump);

       

        if (controller.isGrounded)
        {
            jumpTimeCounter = jumpTime;
        }

        if (transform.position.y < -30)
        {
            GetComponent<PlayerCombat>().Die();
        }

    }

    public void ResetPosition()
    {
        transform.position = startPosition;
    }



}
