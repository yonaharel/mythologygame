﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public float transitionTime = 1;
    public Animator transition;
    [SerializeField] private GameObject hero;
    // Update is called once per frame


   [SerializeField] private GameObject canvas;
    private GameObject eventSystem;
    private GameObject itemAssets;
    private AudioManager audioManager;
    private GameObject gameMaster;
    public bool m_finishedCutScene = false;


    private void Start()
    {


        //if(hero == null)
        //    hero = GameObject.Find("Hero");

        //canvas = GameObject.Find("MainCanvas");

        hero = GameMaster.GM.hero;
        canvas = GameMaster.GM.canvas;


        eventSystem = GameObject.Find("EventSystem");
        itemAssets = GameObject.Find("ItemAssets");
        audioManager = FindObjectOfType<AudioManager>();
        gameMaster = GameObject.Find("GameMaster");
    }

 

    public void LoadNextLevel()
    {
        DontDestroyOnLoad(GameObject.Find("MainCanvas"));
        DontDestroyOnLoad(hero);
        DontDestroyOnLoad(GameObject.Find("EventSystem"));
        DontDestroyOnLoad(GameObject.Find("ItemAssets"));
        DontDestroyOnLoad(FindObjectOfType<AudioManager>());
        DontDestroyOnLoad(GameObject.Find("GameMaster"));

        canvas.SetActive(false);
        hero.SetActive(false);
    
      StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }


    public void LoadFirstLevel()
    {

    

        DestroyAll();



        StartCoroutine(LoadLevel(1));

    }

    public void FinishCutScene()
    {


        m_finishedCutScene = true;
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
        

    }


    IEnumerator LoadLevel(int levelIndex)
    {
        //play animation
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        
        SceneManager.LoadScene(levelIndex);

        if (m_finishedCutScene)
        {
            canvas.SetActive(true);
            hero.SetActive(true);
        }
        if (levelIndex == 4)
        {
            hero.transform.position = GameMaster.GM.egyptStartPosition;
        } else if(levelIndex == 6)
        {
            hero.transform.position = GameMaster.GM.greekStartPosition;
        }
        else hero.transform.position = new Vector3(0f, 0f, 0f);



    }


    public void DestroyAll()
    {
        foreach (var root in itemAssets.scene.GetRootGameObjects())
            Destroy(root);
    }
    

}
