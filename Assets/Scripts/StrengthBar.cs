﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StrengthBar : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;

    public void SetStrength(int strength)
    {
        slider.value = strength;

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    public void SetMaxStrength(int strength)
    {
        slider.maxValue = strength;
        slider.value = strength;

        fill.color = gradient.Evaluate(1f);
    }
}

