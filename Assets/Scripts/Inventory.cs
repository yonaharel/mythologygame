﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Inventory
{
    public List<Item> items;
    //public List<Item> rig;

    public Item[] rig;
    public event EventHandler OnItemListChanged;
    public Inventory()
    {
        
        items = new List<Item>();

        rig = new Item[5];

        for (int i = 0; i < rig.Length; i++)
        {
            rig[i] = null;
        }

        //AddItem(new Item { itemType = Item.ItemType.Sword, amount = 1 });
    }

    public void AddItem(Item item)
    {
        //Item newItem = new Item();
        //newItem = item;
        items.Add(item);
        
        GameMaster.GM.items = items;
        
        OnItemListChanged?.Invoke(this, EventArgs.Empty);

    }

    public List<Item> GetItems()
    {
        return items;
    }

    public void RemoveItem(int index)
    {
        items.RemoveAt(index);
        GameMaster.GM.items = items;
     
    }


    public void RemoveItemByReference(Item item)
    {
        items.Remove(item);
    }

    public void AddItemToRig(Item item) {

        int itemIndex = (int)item.itemRole;
    
        if(rig[itemIndex] != null)
        {
            items.Add(rig[itemIndex]);
        }

        rig[itemIndex] = item;
    }

}
