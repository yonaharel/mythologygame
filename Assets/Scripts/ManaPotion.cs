﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ManaPotion : Item
{

    public int manaBonus;

    public override void Use()
    {
        base.Use();
        player.getBonusMana(manaBonus);
    }
}
