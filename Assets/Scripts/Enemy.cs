﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

public class Enemy : MonoBehaviour
{
    public int maxHealth = 100;
    int currentHealth;
    public Animator animator;
    public bool ranged = false;

    public GameObject itemDrop;
    public bool m_isDead = false;

    public GameObject blood; 

    public enum EnemyType: int
    {
        ICEMAN,
        DEMON,
        SKELETON,
        HELL_BEAST,
        AXE_DEMON,
        MINOTAUR,
        WOLF,
        PATROL_WITCH,
        RANGED_STILL_WITCH,
        GOLEM
    }

    public EnemyType enemyType;
    
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;

        if (!animator)
        {
            animator = GetComponent<Animator>();
        }


        if (!blood)
        {
           blood = GameMaster.GM.blood;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int damage)
    {
        if (m_isDead) return;

        if (blood)
        {
            Instantiate(blood, transform.position, Quaternion.identity);
        }

        switch (enemyType)
        {
            case EnemyType.ICEMAN:
                FindObjectOfType<AudioManager>().Play("EnemyHurt");
                break;
            case EnemyType.DEMON:
                FindObjectOfType<AudioManager>().Play("EnemyHurt2");
                break;
            case EnemyType.SKELETON:
                FindObjectOfType<AudioManager>().Play("EnemyHurt3");
                break;
            case EnemyType.HELL_BEAST:
                FindObjectOfType<AudioManager>().Play("EnemyHurt4");
                break;
            case EnemyType.AXE_DEMON:
                FindObjectOfType<AudioManager>().Play("EnemyHurt5");
                break;
            case EnemyType.MINOTAUR:
                FindObjectOfType<AudioManager>().Play("EnemyHurt2");
                break;
            case EnemyType.WOLF:
                FindObjectOfType<AudioManager>().Play("EnemyHurt3");
                break;
            case EnemyType.PATROL_WITCH:
            case EnemyType.RANGED_STILL_WITCH:
                FindObjectOfType<AudioManager>().Play("EnemyHurt4");
                break;
            case EnemyType.GOLEM:
                FindObjectOfType<AudioManager>().Play("EnemyHurt6");
                break;
            default:
                Debug.Log("Nothing");
                break;
        }
        currentHealth -= damage;

        

        animator.SetTrigger("Hurt");

        if (currentHealth <= 0)
        {
            m_isDead = true;
            GetComponent<Collider2D>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 0f;
            Die();
        }
    }




    void Die()
    {
        switch(enemyType)
        {
            case EnemyType.ICEMAN:
                FindObjectOfType<AudioManager>().Play("EnemyDeath");
                break;
            case EnemyType.DEMON:
                FindObjectOfType<AudioManager>().Play("EnemyDeath2");
                break;
            case EnemyType.SKELETON:
                FindObjectOfType<AudioManager>().Play("EnemyDeath3");
                break;
            case EnemyType.HELL_BEAST:
                FindObjectOfType<AudioManager>().Play("EnemyDeath4");
                break;
            case EnemyType.AXE_DEMON:
                FindObjectOfType<AudioManager>().Play("EnemyDeath5");
                break;
            case EnemyType.MINOTAUR:
                FindObjectOfType<AudioManager>().Play("EnemyDeath2");
                break;
            case EnemyType.WOLF:
                FindObjectOfType<AudioManager>().Play("EnemyDeath3");
                break;
            case EnemyType.PATROL_WITCH:
            case EnemyType.RANGED_STILL_WITCH:
                FindObjectOfType<AudioManager>().Play("EnemyDeath4");
                break;
            case EnemyType.GOLEM:
                FindObjectOfType<AudioManager>().Play("EnemyDeath6");
                break;
            default:
                Debug.Log("Nothing");
                break;
        }

        if (ranged)
            GetComponent<EnemyRangedAttack>().enabled = false;
        else GetComponent<EnemyMeleeAttack>().enabled = false;

        if (itemDrop)
            DropItem();

        Patrol patrol = GetComponent<Patrol>();
        if (patrol)
        {
            patrol.enabled = false;
        }

        Debug.Log("Enemy Died!");
        //Die Animation
        animator.SetTrigger("Die");

      
        
    }


    void DropItem()
    {

        Debug.Log("enemy dropping item");
        Vector3 dropPosition = new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z);
        Instantiate(itemDrop,dropPosition, transform.rotation);
        itemDrop = null;
        
           
    }

    
}
