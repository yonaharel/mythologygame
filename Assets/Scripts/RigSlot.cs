﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RigSlot : MonoBehaviour
{

    private Item item;
    public Image icon;


    public void AddItem(Item newItem)
    {
        //if(item != null)
        //{
        //    GameObject.Find("Hero").GetComponent<PlayerCombat>().AddToInvenotry(item);
        //}


        item = newItem;
        if (item)
            Debug.Log("item in slot is " + item.name);

        icon.sprite = item.GetSprite();
        icon.enabled = true;

    }

    public void ClearSlot()
    {
        if (item)
        {
            Debug.Log("clearing slot of item " + item.name);
        }
        item = null;
        icon.sprite = null;
        icon.enabled = false;
    }
}
