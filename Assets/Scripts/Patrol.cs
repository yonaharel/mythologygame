﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{

    public Transform groundDetection;
    public Transform wallDetection;
    public Transform playerTarget;
    public bool facingRight = true;
    public bool ranged = false;
    public float attackDistance;
    public float speed;
    public EnemyMeleeAttack meleeAttack;
   
    private Rigidbody2D rb;
    private EnemyRangedAttack rangedAttack;
    private Animator animator;
    //private bool movingRight = true;
    private float distanceFromPlayer;
    bool m_attackingPlayer = false; 

    // Start is called before the first frame update
    void Start()
    {
        rangedAttack = GetComponent<EnemyRangedAttack>();
        meleeAttack = GetComponent<EnemyMeleeAttack>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        if (!playerTarget)
        {
            playerTarget = GameObject.Find("Hero").transform;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        distanceFromPlayer = Vector3.Distance(transform.position, playerTarget.position);
        if (distanceFromPlayer < attackDistance)
        {
            if (!m_attackingPlayer)
                AttackPlayer();
            return;
        }

        else
        {
            m_attackingPlayer = false;
            if (ranged)
                rangedAttack.enabled = false;
            else meleeAttack.enabled = false;
        }

        transform.Translate(Vector2.right * speed * Time.deltaTime);
        animator.SetFloat("Speed", speed);

        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, 2f);
        RaycastHit2D wallInfo = Physics2D.Raycast(wallDetection.position, Vector2.right, .5f);
        //check if we hit obstacle or if there is no ground beneath
        if (!groundInfo.collider|| (wallInfo.collider && wallInfo.collider.gameObject != gameObject)  || groundInfo.collider.gameObject.layer == 4)
        {
           
            

            if (facingRight)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                facingRight = false;
                
            } else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                facingRight = true;
            }
        }
    }


    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Rotate player
        transform.Rotate(0f, 180f, 0f);

        Debug.Log("flipping player");
    }



    void AttackPlayer()
    {

        m_attackingPlayer = true;
        animator.SetFloat("Speed", 0.0f);
        //if player to the left of us
        if (playerTarget.position.x < transform.position.x)
        {
            // Switch the way the player is labelled as facing.
            if (facingRight)
                Flip();
        }
        else //player to the right of us
        {
            if (!facingRight)
                Flip();
        }

        //range attack or melee attack on player is enabled  
        if (ranged)
        {

            rangedAttack.enabled = true;
        }
        else
        {
            meleeAttack.enabled = true;
        }

    }

}
