﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ButtonPressed : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent actionWhenPressed;
    public UnityEvent actionWhenReleased;
    bool isPressed = false;
  
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        isPressed = true;
    }


    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        actionWhenReleased.Invoke();
        isPressed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPressed)
        {
            
            return;
        }
        actionWhenPressed.Invoke();
    }
}
