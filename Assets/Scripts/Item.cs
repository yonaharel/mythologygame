﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;



public class Item : MonoBehaviour
{

    public Sprite icon;
    [SerializeField] protected PlayerCombat player;
    [SerializeField] private GameObject hero;


    private void Awake()
    {

        //player = FindObjectOfType<PlayerCombat>();
        hero = GameObject.Find("Hero");
        if (hero)
        {
            player = hero.GetComponent<PlayerCombat>();
        }
    }

    public void PickedUp()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Dropped()
    {
        SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
    }

    public enum ItemType
    {
        Sword,
        nordicSword,
        egyptianSword,
        Shield,
        Helmet,
        HealthPotion,
        ManaPotion,
        StaminaPotion,
        IceBullet,
        NoType
    }

    public enum ItemRole
    {  
        Weapon,
        Shield,
        Armor,
        Helmet,
        Ability,
        Consumable,
    }


    public ItemType itemType;

    public ItemRole itemRole;
    public int amount;


    public virtual void Use()
    {
        string soundName;
        if(itemRole == ItemRole.Consumable)
        {
            soundName = "Potion Drink"; 
        } else
        {
            soundName = "Item Equip";
        }

        FindObjectOfType<AudioManager>().Play(soundName);
    }
     
    public Item GetItem()
    {
        return this;
    }

    public Sprite GetSprite()
    {
        if (icon) return icon;


        switch (itemType)
        {
            default:
            case ItemType.Sword: return ItemAssets.Instance.swordSprite;
            case ItemType.HealthPotion: return ItemAssets.Instance.healthPotionSprite;
            case ItemType.ManaPotion: return ItemAssets.Instance.manaPotionSprite;
            case ItemType.StaminaPotion: return ItemAssets.Instance.staminaPotionSprite;
            case ItemType.nordicSword: return ItemAssets.Instance.nordicSword;
            case ItemType.IceBullet: return ItemAssets.Instance.IceBullet;
            case ItemType.NoType: return icon;
        }
    }



}
