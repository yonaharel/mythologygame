﻿using UnityEngine;
using System.Collections;

public class BasicCameraFollow : MonoBehaviour 
{

	private Vector3 startingPosition;
	[SerializeField] private Transform followTarget;
	private Vector3 targetPos;
	public float moveSpeed;
	
	void Start()
	{

		if(!followTarget)
	    followTarget = GameObject.Find("CameraTarget").transform;
		
		startingPosition = transform.position;
	}


    void Update () 
	{
	

		if (followTarget != null)
		{
			targetPos = new Vector3(followTarget.position.x, followTarget.position.y + 1.5f, transform.position.z);
			Vector3 velocity = (targetPos - transform.position) * moveSpeed;
			transform.position = Vector3.SmoothDamp (transform.position, targetPos, ref velocity, 1.0f, Time.deltaTime);
		}
	}
}

