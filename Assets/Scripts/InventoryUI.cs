﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class InventoryUI : MonoBehaviour
{
    private Inventory inventory;
    public Transform itemsParent;
    public Transform rigParent;
    public GameObject inventoryUI;
    InventorySlot[] slots;
    RigSlot[] rigSlots;




    private void Awake()
    {

     
        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
        rigSlots = rigParent.GetComponentsInChildren<RigSlot>();
    }


    public void SetInventory (Inventory inventory)
    {
        this.inventory = inventory;
        RefreshInventoryItems();
        inventory.OnItemListChanged += Inventory_OnItemListChanged;

        inventoryUI.SetActive(false);
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }

    public void RefreshInventoryItems()
    {

        List<Item> items = inventory.GetItems();

        for(int i = 0; i < slots.Length; i++)
        {
            if (i < items.Count)
            {
                //for (int j = 0; j < i; j++)
                //{
                //    //increasing items count if we encountered the same item.
                //    if (inventory.items[i].itemType == inventory.items[j].itemType)
                //    {
                //        inventory.items[j].amount++;
                //        slots[i].UpdateAmount();
                //    }
                //    else
                //    {
                //        slots[i].AddItem(inventory.items[i]);
                //    }
                //}
                slots[i].AddItem(items[i]);
            
            } else
            {
                slots[i].ClearSlot();
            }

            slots[i].SetSlotNumber(i);
        }




        for (int i = 0; i < inventory.rig.Length; i++)
        {
            if(inventory.rig[i] != null)
            {
                rigSlots[i].AddItem(inventory.rig[i]);
            }
        }

        

    }

    public void OpenInventory()
    {
        //Debug.Log("opening inventory or closing invenotyr");

        if (inventoryUI.activeSelf)
        {

            for (int i = 0; i < slots.Length; i++)
            {
                slots[i].DeselectSlot();
            }


            inventoryUI.SetActive(false);
            FindObjectOfType<AudioManager>().Play("Open Inventory");




        }
        else
        {
            inventoryUI.SetActive(true);
            FindObjectOfType<AudioManager>().Play("Close Inventory");

        }

    }


    public void UseButtonClicked()
    {
        
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].isSelected)
            {
                slots[i].UseItem();
                slots[i].ClearSlot();
                inventory.RemoveItemByReference(slots[i].item);
            }
        }

        RefreshInventoryItems();
    }


    public void DropButtonClicked()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].isSelected)
            {
                slots[i].DropItem();
                slots[i].ClearSlot();
                inventory.RemoveItemByReference(slots[i].item);
            }
        }
        RefreshInventoryItems();

    }


}
