﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{
    // Start is called before the first frame update
    public int healthBonus = 15;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("hit " + collision.gameObject.name);
        if (collision.gameObject.name == "Hero")
        {
            PlayerCombat player = collision.gameObject.GetComponent<PlayerCombat>();

            player.GetBonusHealth(healthBonus);
                
            Destroy(gameObject);
        }
    }
}
