﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerCombat : MonoBehaviour
{
    enum SkillType { fireBall, icePick, none }

    public Animator animator;
    public Transform attackPoint;

    public GameObject impactEffect;
    public LineRenderer lineRenderer;
    public float attackRange = .5f;

    //public List<GameObject> skills;
    public Sword sword;
    public GameObject SwordArm;
    public int attackDamage = 20;
    public float attackRate = 2f;
    private float nextAttackTime = 0f;

    public LayerMask enemiesLayer;

    public int maxHealth = 100;
    public float armorDefense = 0;
    public int currentHealth;
    public int maxMana = 100;
    public int currentMana;
    public int manaCost = 20; 
    public int maxStrength = 100;
    public int currentStrength;
    [SerializeField] private GameObject currentSkill;

    public HealthBar healthBar;
    public ManaBar manaBar;
    public StrengthBar strengthBar;

    float currentTime = 0f;
    float startingTime = 10f;

    float currentTimeStrength = 0f;
    float startingTimeStrength = 7f;

    private Scene scene;
    private Inventory inventory;
    ///[SerializeField] InventoryUI inventoryUI;
    public InventoryUI inventoryUI;
    public Transform itemsParent;


    private Vector3 startingPosition;

    AudioManager audioManager;


    [SerializeField] private float pickUpDelay = 0.25f;
    private float nextPickUpTime = 0 ; 

    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        currentMana = maxMana;
        manaBar.SetMaxMana(maxMana);
        currentStrength = maxStrength;
        strengthBar.SetMaxStrength(maxStrength);
        currentTime = startingTime;
        currentTimeStrength = startingTimeStrength;
        scene = SceneManager.GetActiveScene();

        inventory = new Inventory();
        inventoryUI.SetInventory(inventory);

        inventory.items = GameMaster.GM.items;
        if(inventory.items.Count > 0)
        {
            inventoryUI.RefreshInventoryItems();
        }

        startingPosition = transform.position;

        audioManager = FindObjectOfType<AudioManager>();

    }

    // Update is called once per frame
    void Update()
    {

       

        currentTime -= 1 * Time.deltaTime;
        currentTimeStrength -= 1 * Time.deltaTime;

        // If the timer gets to 0, give the hero 5 mana and restart the counter
        if (currentTime <= 0)
        {
            currentMana += 5;
            manaBar.SetMana(currentMana);
            currentTime = 10f;
        }

        // If the strength timer gets to 0, give the hero 5 health anmd restart the counter
        if (currentTimeStrength <= 0)
        {
            currentStrength += 5;
            strengthBar.SetStrength(currentStrength);
            currentTimeStrength = 3f;
        }

        if (Input.GetButtonDown("Fire1") && currentMana >= manaCost)
        {
            Shoot();
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            FindObjectOfType<LevelLoader>().LoadNextLevel();
        }



    }

    void Shoot()
    {
    
        //if player has an ability equipped 
        if (currentSkill)
        {
            animator.SetTrigger("Attack");
            FindObjectOfType<AudioManager>().Play("PlayerUseSpell");
            currentMana -= manaCost;

            Bullet bullet = currentSkill.GetComponent<Bullet>();
            bullet.bulletDirection = attackPoint.right;
            Instantiate(currentSkill, attackPoint.position, attackPoint.rotation);
        }

    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (hitInfo.gameObject.tag == "Items")
        {
           
            if ( Time.time >= nextPickUpTime)
            {

                if (inventory.items.Count < 15)
                {
                    audioManager.Play("Item Pickup");
                    Item item = hitInfo.gameObject.GetComponent<Item>().GetItem();

                    item.PickedUp();
                    hitInfo.gameObject.SetActive(false);
                    AddToInvenotry(item);
                }
                nextPickUpTime = Time.time + pickUpDelay;
                
            }
            

            return;
        }

        if (hitInfo.gameObject.layer == 4)  // we hit water
        {
            takeDamage(50);
            return;
        }

        if(hitInfo.gameObject.tag == "Finish")
        {
            FindObjectOfType<LevelLoader>().LoadNextLevel();
            return;
        }

        if (hitInfo.gameObject.name == "topOfSlope")
        {
            Rigidbody2D rb = GetComponent<Rigidbody2D>();
            rb.velocity = new Vector3(rb.velocity.x, 0f);
            return;
        }

        Destroy(hitInfo.gameObject);
    }

    void Attack()
    {


        attackDamage = sword.attackDamage;
        //play Attack animation and move the sword
        animator.SetTrigger("Attack");

        FindObjectOfType<AudioManager>().Play("PlayerAttack");


        //attack with the collider
        Collider2D collider = Physics2D.OverlapCircle(attackPoint.position, attackRange, enemiesLayer);
        if (collider)
        {

            Enemy enemy = collider.gameObject.GetComponent<Enemy>();
            FindObjectOfType<AudioManager>().Play("Sword Slice"); 
            enemy.TakeDamage(attackDamage);
            Debug.Log("hit player");
        }



    }

    public void takeDamage(int damage)
    {

        float floatDamage = (float)damage - (float)damage * armorDefense;
        int reduced = (int)floatDamage;

        currentHealth -= reduced;
        animator.SetTrigger("Hurt");
        
        FindObjectOfType<AudioManager>().Play("PlayerTakeDamage");
      

        //check if we die lol

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public void Die()
    {

        GameObject.Find("Buttons").SetActive(false);
        FindObjectOfType<FixedJoystick>().enabled = false;
        
        //reseting health and Starting Position of the player
        animator.SetTrigger("Dying");
        animator.SetBool("isDead", true);
        //PlayerDeath
        FindObjectOfType<AudioManager>().Play("PlayerDeath");
       // StartCoroutine("OnCompleteDyingAnimation");
        StartCoroutine(OnCompleteDyingAnimation());
        
    }

    public void MeleeAttackButtonClicked()
    {
        // Check if the hero has any strength
        if (currentStrength > 0)
        {
            if (Time.time >= nextAttackTime)
            {
                Attack();
                nextAttackTime = Time.time + 1f / attackRate;
                //Sword sword = GetComponent<Sword>();
                //sword.enabled = true; 

                // When the player attacks, lower their strength
                currentStrength -= 5;
                strengthBar.SetStrength(currentStrength);
            }
        }
    }

    public void SkillAttackButtonClicked()
    {
        // Check if hero has any mana
        if (currentMana > 0)
        {
            Shoot();
            // Remove 5 mana every time the hero uses a skill
            currentMana -= 5;
            manaBar.SetMana(currentMana);
        }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    

    }


    public void GetBonusHealth(int bonus)
    {
        Debug.Log("Adding bonus health of " + bonus + " to player");

        GetComponent<AnimationController>().PlayHealAnimation();

        currentHealth += bonus;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }


    }

    public void getBonusMana(int bonus)
    {
        Debug.Log("Adding bonus mana of " + bonus + " to player");

        currentMana += bonus;
        if(currentMana > maxMana)
        {
            currentMana = maxMana;
        }

        manaBar.SetMana(currentMana);
    }


    public void GetStamina(int staminaBonus)
    {
        currentStrength += staminaBonus;
        if (currentStrength > maxStrength)
        {
            currentStrength = maxStrength;
        }

        strengthBar.SetStrength(currentStrength);
    }

    public void AddToInvenotry(Item item)
    {
        Debug.Log("added item " + item.name);

        inventory.AddItem(item);
        
    }


    public void OpenInventory()
    {
        inventoryUI.OpenInventory();
    }

    IEnumerator OnCompleteDyingAnimation()
    {
    
        yield return new WaitForSeconds(2);

        animator.enabled = false;

        Debug.Log("build Index: " + scene.buildIndex);
        scene = SceneManager.GetActiveScene();
        if(scene.buildIndex > 2)
        {
            currentHealth = 100;
        }

        FindObjectOfType<LevelLoader>().LoadFirstLevel();

        //Destroy();
        
    }

    public void SetSword(Sword sword, SwordItem swordItem)
    {
        this.sword = sword;
        inventory.AddItemToRig(swordItem);
        SwordArm.GetComponent<SpriteRenderer>().sprite = sword.realSizeSword;
        
    }

    public void SetAbility(GameObject abilityPrefab, Item ability)
    {
        currentSkill = abilityPrefab;
        inventory.AddItemToRig(ability);
    }

    public void SetArmor(string armorName, ArmorItem armor)
    {
        GetComponent<ArmorSwitcher>().SetArmor(armorName);
        inventory.AddItemToRig(armor);
        armorDefense = armor.defense;
    }

}
