﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsManager
{
    public static ItemsManager Instance { get; private set; }

    public List<Item> currentItems;


    void updateCurrItems(List<Item> items)
    {

        currentItems = items;
    }

    List<Item> GetItems()
    {
        return currentItems;
    }


}
