﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveToMenu : MonoBehaviour
{

    public float waitTime = 15f;


    private void Update()
    {
        StartCoroutine(FinishIntro());
    }

    IEnumerator FinishIntro()
    {
        yield return new WaitForSeconds(waitTime);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
