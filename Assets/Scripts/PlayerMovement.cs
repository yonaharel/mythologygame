﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float runSpeed = 50f;
    public Animator animator;
    public Joystick joystick;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    public CharacterController2D controller;
    private Vector3 startPosition;


    private float jumpTimeCounter;
    public float jumpTime;

    void Start()
    {
        startPosition = transform.position;
        //Physics2D.IgnoreLayerCollision(8, 13, true);

        Debug.Log("Start Player");

        if(!controller)
            controller = GetComponent<CharacterController2D>();

    }

    // Update is called once per frame
    void Update()
    {
        //Input
        horizontalMove = joystick.Horizontal * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (transform.position.y < -50)
        {
            GetComponent<PlayerCombat>().Die();
        }
    }

    public void OnLanding()
    {
        animator.SetBool("isJumping", false);
    }

    public void JumpButtonClicked()
    {
        Debug.Log("jump clicked");
        animator.SetBool("isJumping", true);
       
        if (controller.IsGrounded())
            jump = true;
        else
        {
            jump = false;
        }
    }

    public void JumpButtonPressed()
    {
        Debug.Log("hodling jump button");


        if (controller.IsGrounded() && !jump)
        {
            FindObjectOfType<AudioManager>().Play("PlayerJump");

            jump = true;
        }

        if (jump)
        {
            if(jumpTimeCounter > 0)
                jumpTimeCounter -= Time.deltaTime;
            else jump = false;
        }
    }

    public void JumpButtonReleased()
    {

        Debug.Log("released jump button");
        jump = false;
    }

    public void CrouchButtonPressed()
    {
        crouch = true;
        
    }

    public void CrouchButtnoReleased()
    {
        crouch = false;
    }


    private void FixedUpdate()
    {
        animator.SetBool("isCrouching", crouch);

        //animator.SetBool("isJumping", jump);


        if (!controller.IsGrounded())
        {
            animator.SetBool("isJumping", true);
        } else
        {
            animator.SetBool("isJumping", false);
        }
       
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
    
        if (controller.IsGrounded())
        {
            jumpTimeCounter = jumpTime;
        }

       

    }

    public void ResetPosition()
    {
        transform.position = startPosition;
    }

    public void Step()
    {
        int levelIndex = SceneManager.GetActiveScene().buildIndex;
        switch (levelIndex)
        {
            default: break;
            case 2: FindObjectOfType<AudioManager>().Play("Snow Step");
                    break;
            case 4: FindObjectOfType<AudioManager>().Play("Sand Step");
                    break;
            case 6: FindObjectOfType<AudioManager>().Play("Ground Step");
                    break;
        }
      
    }
    


}
