﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorItem : Item
{
    public string armorName;

    public float defense = 0.15f;

   
    public override void Use()
    {

        base.Use();
        player.SetArmor(armorName, this);

    }
}
