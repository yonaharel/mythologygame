﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;
using UnityEngine.UI;

public class ArmorSwitcher : MonoBehaviour
{
    [SerializeField] SpriteResolver spriteResolver;

   

    public void SetArmor(string armorName)
    {
        spriteResolver.SetCategoryAndLabel("Armor", armorName);
    }
}
