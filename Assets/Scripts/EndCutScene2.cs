﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndCutScene2 : MonoBehaviour
{

    float countdown = 0f;
    // Start is called before the first frame update
    void Start()
    {
        countdown = 8f;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= 1 * Time.deltaTime;

        if (countdown <= 0)
        {
           FindObjectOfType<LevelLoader>().LoadNextLevel();
        }
    }
}
