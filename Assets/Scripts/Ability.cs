﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : Item
{
    public GameObject abilityPrefab;

    public override void Use()
    {
        //base.Use();

        player.SetAbility(abilityPrefab, this);

    }
}
