﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class HealthPotion : Item
{

    public int healthBonus;

    public override void Use()
    {
        base.Use();
        player.GetBonusHealth(healthBonus);
    }
}