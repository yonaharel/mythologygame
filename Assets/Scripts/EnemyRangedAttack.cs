﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangedAttack : MonoBehaviour
{

    public Transform shootingPoint;
    [SerializeField] private Transform target; 
   
    public Animator animator;
    public GameObject projectilePrefab;
    private Bullet bullet;

    public int attackDamage = 15;

    public LayerMask layersForBulletToIgnore;

    private float nextShootTime = 0;
    public float fireRate = 0.5f;


    // Start is called before the first frame update
    void Start()
    {
        bullet = projectilePrefab.GetComponent<Bullet>();
        bullet.thingsToIgnore = layersForBulletToIgnore;

        target = GameObject.Find("head").transform;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if ( Time.time > nextShootTime ) { 

            Vector3 dir = (target.position - transform.position).normalized;
            Shoot(dir);     
             nextShootTime = Time.time + fireRate;
         }

    }


    public void Shoot(Vector3 shootingDirection)
    {
      
        animator.SetTrigger("Fire");
        FindObjectOfType<AudioManager>().Play("EnemyAttack");
        bullet.bulletDirection = shootingDirection; 
        projectilePrefab.transform.rotation = shootingPoint.rotation;
        Instantiate(projectilePrefab, shootingPoint.position, shootingPoint.rotation);
    }
        
}
