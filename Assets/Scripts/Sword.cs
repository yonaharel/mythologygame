﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public int attackDamage = 10;
    public LayerMask enemyLayers;
    public Sprite realSizeSword;

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("OnCollisionEnter2D");

        Enemy enemy = col.gameObject.GetComponent<Enemy>();
        if (enemy)
        {
            enemy.TakeDamage(attackDamage);
        }

    }
}
