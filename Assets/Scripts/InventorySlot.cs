﻿using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Item item;
    public Image icon;
    public Image selected;
    public Text itemsAmount;
    public int slotNumber;
    public bool isSelected = false;
    

    public void AddItem(Item newItem)
    {
       
        //if (item.amount > 0)
        //{
        //    itemsAmount.enabled = true;
        //    itemsAmount.text = item.amount.ToString();
        //}
        //else if (item.amount == 0)
        //{                                     
        //    itemsAmount.enabled = false;
        //}                                     
        item = newItem;
       
        icon.sprite = item.GetSprite();
        icon.enabled = true;
   
    }

    public void SetSlotNumber(int number)
    {
        slotNumber = number;
    }

    public void ClearSlot()
    {
       
        //item = null;
        icon.sprite = null;
        icon.enabled = false;
        itemsAmount.enabled = false;
        selected.enabled = false;
        isSelected = false; 
    }

    public void UpdateAmount()
    {
        if (item.amount > 1)
        {
            itemsAmount.enabled = true;
            itemsAmount.text = item.amount.ToString();
        }

        if (item.amount == 1)
            itemsAmount.enabled = false;

        if (item.amount == 0)
        {
            ClearSlot();
        }
    }

    public void OnClickItem()
    {
       
        if (item)
        {
            FindObjectOfType<AudioManager>().Play("Item Button Click");

            if (!isSelected)
            {
              

                selected.enabled = true;
                isSelected = true;
            }
            else
            {
                selected.enabled = false;
                isSelected = false;
            }
        }
    }

    public void UseItem()
    {
        DeselectSlot();

        Debug.Log("using item " + item.name);
        item.Use();
        
    }

    public void DeselectSlot()
    {
        if (item)
        {
            selected.enabled = false;
            isSelected = false;
        }
    }


    public void DropItem()
    {

        if (!item) return;

        DeselectSlot();

        Transform hero = GameObject.Find("Hero").transform;

        Vector3 dropPosition = new Vector3(hero.position.x - 2f, hero.position.y, hero.position.z);

        item.gameObject.transform.position = dropPosition;
        item.gameObject.SetActive(true);
        item.Dropped();

       
      
    }

    public Item GetItem() { return item; }

}
